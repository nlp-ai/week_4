


def read_file(path):
	with open(path) as f:
		content = f.readlines()
	# you may also want to remove whitespace characters like `\n` at the end of each line
	content = [x.strip() for x in content] 
	return content

def write_file(lines, path):
	with open(path, 'w') as the_file:
		for l in lines:
			the_file.write(l + '\n')


def preprocess(in_path, src_path, tgt_path):
	content = read_file(in_path)
	src_lines = []
	tgt_lines = []
	for l in content:
		l = l.replace('&apos;', "")

		try:
			data = l.split('\t')
			if len(data) == 2:
				src_lines.append(data[0])
				tgt_lines.append(data[1])
		except:
			print('Error: ', l)

	write_file(src_lines, src_path)
	write_file(tgt_lines, tgt_path)


if __name__ == '__main__':
	in_paths = [
		'dataset/en_vi/train.txt',
		'dataset/en_vi/test.txt',
		'dataset/en_vi/val.txt'] 
	src_paths = [
		'dataset/en_vi/src_train.txt',
		'dataset/en_vi/src_test.txt',
		'dataset/en_vi/src_val.txt'] 
	tgt_paths = [
		'dataset/en_vi/tgt_train.txt',
		'dataset/en_vi/tgt_test.txt',
		'dataset/en_vi/tgt_val.txt'] 

	for i in range(3):
		in_path = in_paths[i]
		src_path = src_paths[i]
		tgt_path = tgt_paths[i]
		preprocess(in_path, src_path, tgt_path)
