from io import open
import unicodedata
import string
import re
import random

import torch
import torch.nn as nn
from torch import optim
import torch.nn.functional as F


file_path = 'dataset/en_vi/test.txt'
lines = open(file_path, encoding='utf-8').\
		read().strip().split('\n')
lines = [''.join(x if x.isalpha() else ' ' for x in s) for s in lines]
print(random.choice(lines))